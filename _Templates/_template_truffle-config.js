const HDWalletProvider = require('truffle-hdwallet-provider');
const _providerName_rpcUrl = "_provider_rpcUrl";
const _providerName_networkID = "_provider_networkID";
const _providerName_mnemonic = "_provider_menemonic";

module.exports = {
  networks: {
    development: {
      host: "127.0.0.1",
      port: 7545,
      network_id: "*"
    },
    _providerName_: {
      provider: () => new HDWalletProvider(_providerName_mnemonic, _providerName_rpcUrl),
      port: 7545,
      network_id: _providerName_networkID,
      gas: 700000000,
      gasPrice:0
    }
  },

  mocha: {},

  compilers: { solc: {} }
}
