const _EntityAssetType_ = artifacts.require("_EntityAssetType_");

module.exports = function(deployer) {
  deployer.deploy(_EntityAssetType_);
};
