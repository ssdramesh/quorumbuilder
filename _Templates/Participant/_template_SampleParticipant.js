/*
CAUTION:  Partial coverage only!
          Tests for contract functions with modifiers NOT in this test file.
 */
const SampleParticipant = artifacts.require("SampleParticipant");

contract("SampleParticipant", accounts => {

  const DEPLOYER = accounts[0];
  const OWNER_ONE = accounts[1];
  const OWNER_TWO = accounts[2];

  const PARTICIPANT_TYPE = "_Participant";

  let instance;

  beforeEach("Setup Contract for each test", async () => {
    instance = await SampleParticipant.deployed();
  });

  it("getDeployer", async () => {
    let sampleParticipant = instance;
    assert.equal(await sampleParticipant.getDeployer.call(), DEPLOYER, "getDeployer: deployer must be original msg.sender");
  });

  it("getParticipantType", async () => {
    let sampleParticipant = instance;
    assert.equal(await sampleParticipant.getParticipantType.call(), PARTICIPANT_TYPE, "getParticipantType: Participant type must be" + PARTICIPANT_TYPE);
  });

  it("add_Participant", async() => {
    let sampleParticipant = instance;
    assert.equal(
      await sampleParticipant.add_Participant.call(
        OWNER_ONE,
        "John"
        //__testAddParticipantParams
      ), 0, "add_Participant: index mismatch"
    );
  });

  it("get_Participant", () => {
    let sampleParticipant;
    SampleParticipant.deployed()
      .then(instance => {
        instance.add_Participant.call(
          OWNER_ONE,
          "John"
          //__testAddParticipantParams
        );
        sampleParticipant = instance;
      })
      .then(participantID => {
        sampleParticipant.get_Participant.call(0)
          .then(participant => {
            assert.equal(
              participant[0].valueOf(),
              accounts[0],
              "get_Participant: owner mismatch"
            );
            assert.equal(
              participant[1].valueOf(),
              "John",
              "get_Participant: ownerName mismatch"
            );
            //__testGetParticipantAssertAttribute
          })
      });
  });

  it("get_ParticipantByAddress", () => {
    let sampleParticipant;
    SampleParticipant.deployed()
      .then(instance => {
        sampleParticipant = instance;
        sampleParticipant.add_Participant.call(OWNER_ONE,"John");
      })
      .then(participantID => {
        sampleParticipant.get_ParticipantByAddress.call(OWNER_ONE)
          .then(participant => {
            assert.equal(
              participant[0].valueOf(),
              OWNER_ONE,
              "get_ParticipantByAddress: owner mismatch"
            );
            assert.equal(
              participant[1].valueOf(),
              "John",
              "get_ParticipantByAddress: ownerName mismatch"
            );
            //__testGetParticipantByAddressAssertAttribute
          })
      });
  });
});
