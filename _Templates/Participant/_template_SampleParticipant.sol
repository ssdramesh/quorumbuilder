pragma solidity >=0.4.22 <0.6.0;


contract SampleParticipant {

  address deployer;
  uint num_Participants = 0;
  string participantType = "_Participant";
  enum Statuses { Active, Inactive, Archived }
/*
TODO: Any Payable in template?
*/
  struct _Participant {
    address owner;
    string ownerName;
    //__contractStructAttributes
    Statuses status;
  }

  mapping (uint => _Participant) participants;
  mapping (address => uint) addressIndex;

  modifier onlyOwnerOrDeployer(uint _participantID) {
    require( msg.sender == deployer ||
             msg.sender == participants[_participantID].owner );
    _;
  }

  modifier onlyDeployer() {
    require( msg.sender == deployer);
    _;
  }

  event _ParticipantAdded(uint _participantID,
                         address _owner,
                         string _ownerName
                         );
  event _ParticipantUpdated(uint _participantID,
                           address _newOwner,
                           string _newOwnerName
                          );
  event _ParticipantDeactivated(address _owner);
  event _ParticipantReactivated(address _owner);
  event _ParticipantArchived(address _owner);

  constructor()
  public {
    deployer = msg.sender;
  }

  function getDeployer()
  public
  view
  returns(address) {
    return deployer;
  }

  function getParticipantType()
  public
  view
  returns(string memory) {
    return participantType;
  }

  function add_Participant(address _owner,
                          string memory _ownerName
                          //__contractAddParticipantParams
                          )
  public
  returns(uint participantID) {
    participantID = num_Participants++;
    participants[participantID] =
      _Participant(_owner,
                  _ownerName,
                  //__contractAddParticipantNewStruct
                  Statuses.Active
                  );
    addressIndex[_owner] = participantID;
    emit _ParticipantAdded(participantID,
                          _owner,
                          _ownerName
                          );
    return participantID;
  }

  function get_ParticipantCount()
  public
  view
  onlyDeployer()
  returns(uint) {
    return num_Participants;
  }

  function get_Participant(uint _participantID)
  public
  view
  returns(address owner,
          string memory ownerName
          //__contractGetParticipantDeclareReturn
         ) {
    assert(participants[_participantID].status == Statuses.Active);
    return (participants[_participantID].owner,
            participants[_participantID].ownerName
            //__contractGetParticipantReturnAttribute
           );
  }

  function get_ParticipantByAddress(address _address)
  public
  view
  returns(address owner,
          string memory ownerName
          //__contractGetParticipantByAddressDeclareReturn
         ) {
    assert(participants[addressIndex[_address]].status == Statuses.Active);
    return(participants[addressIndex[_address]].owner,
           participants[addressIndex[_address]].ownerName
           //__contractGetParticipantByAddressReturnAttribute
          );
  }

  function update_Participant(uint _participantID,
                             address _newOwner,
                             string memory _newOwnerName
                             //__contractUpdateParticipantParams
                             )
  public
  onlyOwnerOrDeployer(_participantID)
  returns(address owner,
          string memory ownerName
          //__contractUpdateParticipantDeclareReturn
         ) {
      assert(participants[_participantID].status == Statuses.Active);
      participants[_participantID].owner = _newOwner;
      participants[_participantID].ownerName = _newOwnerName;
      //__contractUpdateParticipantAssign
      emit _ParticipantUpdated(_participantID,
                              _newOwner,
                              _newOwnerName
                              );

      return (participants[_participantID].owner,
              participants[_participantID].ownerName
              //__contractUpdateParticipantReturn
             );
  }

  function deactivate_Participant(address _owner)
  public
  onlyDeployer()
  returns(bool) {
    assert(participants[addressIndex[_owner]].status == Statuses.Active);
    participants[addressIndex[_owner]].status == Statuses.Inactive;
    emit _ParticipantDeactivated(_owner);
    return true;
  }

  function reactivate_Participant(address _owner)
  public
  onlyDeployer()
  returns(bool) {
    assert(participants[addressIndex[_owner]].status == Statuses.Inactive);
    participants[addressIndex[_owner]].status == Statuses.Active;
    emit _ParticipantReactivated(_owner);
    return true;
  }

  function archive_Participant(address _owner)
  public
  onlyDeployer()
  returns(bool) {
    assert(participants[addressIndex[_owner]].status == Statuses.Inactive);
    participants[addressIndex[_owner]].status == Statuses.Archived;
    emit _ParticipantArchived(_owner);
    return true;
  }
}
