var fs = require("fs-extra");
var spawn = require("cross-spawn");
var config = require("../configUtil/config.js");

async function installModules(project) {
  try {
    await spawn("npm",["install", "truffle-hdwallet-provider", "--save"], {cwd: project.generationProjectPath, env:process.env});
    dialog.showMessageBox({
      type:"info",
      title: "Quorum Builder Generation",
      message:"Node modules truffle-hdwallet-provider installed for project: \n" + project.generationProjectPath
    });
  } catch(error) {
      dialog.showMessageBox({
        type:"error",
        title: "Quorum Builder Generation",
        message:"Error/Warnings in node modules installation for truffle project: \n" + error.toString()
      });
  }
}
exports.installModules = installModules;
