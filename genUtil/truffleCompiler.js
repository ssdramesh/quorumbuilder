var fs = require("fs-extra");
var spawn = require("cross-spawn");
var config = require("../configUtil/config.js");

async function compile(project){
  try {
    spawn("truffle",["compile"], {cwd: project.generationProjectPath, env:process.env});
    dialog.showMessageBox({
      type:"info",
      title: "Quorum Builder Generation",
      message:"Network is migrated (deployed) to: 'development'"
    });
  } catch(error) {
    dialog.showMessageBox({
      type:"error",
      title: "Quorum Builder Generation",
      message:"Error(s) Occurred: \n" + error.toString()
    });
  }
}

exports.compile = compile;
