var spawn = require("cross-spawn");
var config = require("../configUtil/config");

async function test(project, network) {
  try {
    await spawn("truffle",["test", "--network", network], {cwd: project.generationProjectPath, env:process.env});
    dialog.showMessageBox({
      type:"info",
      title: "Quorum Builder Test",
      message:"Network is successfully tested on network: " + network
    });
  } catch (error) {
    dialog.showMessageBox({
      type:"error",
      title: "Quorum Builder Test",
      message:"Error(s) Occurred: \n" + error.toString()
    });
  }
}
exports.test = test;
