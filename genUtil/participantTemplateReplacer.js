
function __contractStructAttributesTo(attr, idx, parent) {
  return "string " +
         attr.name +
         ";\n\t\/\/__contractStructAttributes";
}

function __contractAddParticipantNewStructTo(attr, idx, parent) {
  return "_" +
         attr.name +
         ",\n\t\/\/__contractAddParticipantNewStruct";
}

function __contractAddParticipantParamsTo(attr, idx, parent) {
  return ",\n\tstring memory _" +
         attr.name +
         "\n\t\/\/__contractAddParticipantParams";
}

function __contractGetParticipantDeclareReturnTo(attr, idx, parent) {
  return ",\n\tstring memory _" +
         attr.name +
         "\n\/\/__contractGetParticipantDeclareReturn";
}

function __contractGetParticipantReturnAttributeTo(attr, idx, parent) {
  return ",\n" + parent.toLowerCase() + "s[_" + parent.toLowerCase() + "ID]." +
         attr.name +
         "\n\/\/__contractGetParticipantReturnAttribute";
}

function __contractGetParticipantByAddressDeclareReturnTo(attr, idx, parent) {
  return ",\n\tstring memory _" +
         attr.name +
         "\n\/\/__contractGetParticipantByAddressDeclareReturn";
}

function __contractGetParticipantByAddressReturnAttributeTo(attr, idx, parent) {
  return ",\n" + parent.toLowerCase() + "s[addressIndex[_address]]." +
         attr.name +
         "\n\/\/__contractGetParticipantByAddressReturnAttribute"
}

function __contractUpdateParticipantParamsTo(attr, idx, parent) {
  return ",\n\tstring memory _new" +
         attr.name +
         "\n\/\/__contractUpdateParticipantParams";
}

function __contractUpdateParticipantDeclareReturnTo(attr, idx, parent) {
  return ",\n\tstring memory " +
         attr.name +
         "\n\/\/__contractUpdateParticipantDeclareReturn";
}

function __contractUpdateParticipantAssignTo(attr, idx, parent) {
  return parent.toLowerCase() + "s[_" + parent.toLowerCase() + "ID]." +
         attr.name +
         " = _new" +
         attr.name +
         " ;" +
         "\n\/\/__contractUpdateParticipantAssign";
}

function __contractUpdateParticipantReturnTo(attr, idx, parent) {
  return ",\n" + parent.toLowerCase() + "s[_" + parent.toLowerCase() + "ID]." +
         attr.name +
         "\n\/\/__contractUpdateParticipantReturn";
}

function __testAddParticipantParamsTo(attr, idx, parent) {
  return ",\n\t\t\t\t\t" +
         "\"" + attr.name + "_001\"" +
         "\n\t\t\t\t\t\/\/__testAddParticipantParams";
}

function __testGetParticipantAssertAttributeTo(attr, idx, parent) {
  return "assert.equal(\n\t\t\t\t\t\t" +
                parent.toLowerCase() + "[" + (2 + idx) + "].valueOf(),\n\t\t\t\t\t\t" +
                "\"" + attr.name + "_001\",\n\t\t\t\t\t\t" +
                "\"get" + parent + ": " + attr.name + " mismatch\"\n\t\t\t\t\t\t\t" +
              ");\n\t\t\t\t\t" +
            "\t\t\t/\/__testGetParticipantAssertAttributeTo";
}

function __testGetParticipantByAddressAssertAttributeTo(attr, idx, parent) {
  return "assert.equal(\n\t\t\t\t\t\t\t" +
                parent.toLowerCase() + "[" + (2 + idx) + "].valueOf(),\n\t\t\t\t\t\t\t" +
                "\"" + attr.name + "_001\",\n\t\t\t\t\t\t\t" +
                "\"get" + parent + "ByAddress: " + attr.name + " mismatch\"\n\t\t\t\t\t\t\t\t" +
              ");\n\t\t\t\t\t" +
            "\t\t\t\/\/__testGetParticipantByAddressAssertAttributeTo";
}

exports.__contractStructAttributesTo = __contractStructAttributesTo;
exports.__contractAddParticipantNewStructTo = __contractAddParticipantNewStructTo;
exports.__contractAddParticipantParamsTo = __contractAddParticipantParamsTo;
exports.__contractGetParticipantDeclareReturnTo = __contractGetParticipantDeclareReturnTo;
exports.__contractGetParticipantReturnAttributeTo = __contractGetParticipantReturnAttributeTo;
exports.__contractGetParticipantByAddressDeclareReturnTo = __contractGetParticipantByAddressDeclareReturnTo;
exports.__contractGetParticipantByAddressReturnAttributeTo = __contractGetParticipantByAddressReturnAttributeTo;
exports.__contractUpdateParticipantParamsTo = __contractUpdateParticipantParamsTo;
exports.__contractUpdateParticipantDeclareReturnTo = __contractUpdateParticipantDeclareReturnTo;
exports.__contractUpdateParticipantAssignTo = __contractUpdateParticipantAssignTo;
exports.__contractUpdateParticipantReturnTo = __contractUpdateParticipantReturnTo;
exports.__testAddParticipantParamsTo = __testAddParticipantParamsTo;
exports.__testGetParticipantAssertAttributeTo = __testGetParticipantAssertAttributeTo;
exports.__testGetParticipantByAddressAssertAttributeTo = __testGetParticipantByAddressAssertAttributeTo;
