var fs = require("fs-extra");
var replace = require("replace-in-file");
var config = require("../configUtil/config");

async function add(entity, idx, project) {
  // Template
  var tmplAssetContract = config.readScriptPathSetting() + "/Asset/_template_SampleAsset.sol";
  var tmplAssetTest = config.readScriptPathSetting() + "/Asset/_template_SampleAsset.js";
  var tmplDeployCtrContent = (await fs.readFile(config.readScriptPathSetting() + "/_template_2_deploy_contracts.js")).toString();

  // Targets
  var trgtCtrFilename = project.generationProjectPath + "/contracts/" + entity.name + entity.stereotype + ".sol";
  var trgtTstFilename = project.generationProjectPath + "/test/" + entity.name + entity.stereotype + ".js";
  var trgtDepCtrFilename = project.generationProjectPath + "/migrations/2_deploy_contracts.js";

  try {
    await fs.copy(tmplAssetContract, trgtCtrFilename);  //Contract
    await fs.copy(tmplAssetTest, trgtTstFilename);      //Test
    await replace({
      files: [trgtCtrFilename, trgtTstFilename],
      from: [/SampleAsset/g, /sampleAsset/g, /_Asset/g, /participant/g],
      to: [entity.name + entity.stereotype, entity.name.toLowerCase() + entity.stereotype, entity.name, entity.name.toLowerCase()],
    });
    // TODO: Add modeled entity attributes
    // add migration for entity
    var trgtDeployCtrContent = tmplDeployCtrContent.replace(/_EntityAssetType_/g, entity.name + entity.stereotype);
    if ( idx === 0 ) {
      await fs.writeFile(trgtDepCtrFilename, trgtDeployCtrContent);
    } else {
      await fs.appendFile(trgtDepCtrFilename,trgtDeployCtrContent);
    }
  } catch (err) {
      dialog.showMessageBox({
        type: "error",
        title: "Quorum Builder Generation",
        message: err.toString()
      });
  }
}
exports.add = add;
