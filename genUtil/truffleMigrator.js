var spawn = require("cross-spawn");
var config = require("../configUtil/config");

async function migrate(project, network) {
  try{
    await spawn("truffle",["migrate", "--network", network], {cwd: project.generationProjectPath, env:process.env});
    dialog.showMessageBox({
      type:"info",
      title: "Quorum Builder Migration (Deployment: 'development')",
      message:"Network is successfully migrated (deployed) to: " + network
    });
  } catch(error) {
    dialog.showMessageBox({
      type:"error",
      title: "Quorum Builder Migration (Deployment: 'development')",
      message:"Error(s) Occurred: \n" + error.toString()
    });
  }
}

exports.migrate = migrate;
