var fs = require("fs-extra");
var spawn = require("cross-spawn");
var config = require("../configUtil/config.js");

async function init(project) {
  try {
    await fs.ensureDir(project.generationProjectPath);
    await spawn("truffle",["init", "--force"], {cwd: project.generationProjectPath, env:process.env});
    dialog.showMessageBox({
      type:"info",
      title: "Quorum Builder Generation",
      message:"Truffle project successfully generated at: " + project.generationProjectPath
    });
  } catch(error) {
      dialog.showMessageBox({
        type:"error",
        title: "Quorum Builder Generation",
        message:"Error in initialization of truffle project; " + error.toString()
      });
  }
}
exports.init = init;
