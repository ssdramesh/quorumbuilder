var fs = require("fs-extra");
var _ = require("underscore");
var replace = require("replace-in-file");
var config = require("../configUtil/config");
var ptr = require("./participantTemplateReplacer");

async function add(entity, idx, project) {
  // Templates
  var tmplParticipantContract = config.readScriptPathSetting() + "/Participant/_template_SampleParticipant.sol";
  var tmplParticipantTest = config.readScriptPathSetting() + "/Participant/_template_SampleParticipant.js";
  var tmplDeployCtrContent = (await fs.readFile(config.readScriptPathSetting() + "/_template_2_deploy_contracts.js")).toString();

  // Targets
  var trgtCtrFilename = project.generationProjectPath + "/contracts/" + entity.name + entity.stereotype + ".sol";
  var trgtTstFilename = project.generationProjectPath + "/test/" + entity.name + entity.stereotype + ".js";
  var trgtDepCtrFilename = project.generationProjectPath + "/migrations/2_deploy_contracts.js";

  var parent = entity.name;

  try {
    await fs.copy(tmplParticipantContract, trgtCtrFilename);  //Contract
    await fs.copy(tmplParticipantTest, trgtTstFilename);      //Test
    await replace({
      files: [trgtCtrFilename, trgtTstFilename],
      from: [/SampleParticipant/g, /sampleParticipant/g, /_Participant/g, /participant/g],
      to: [entity.name + entity.stereotype, entity.name.toLowerCase() + entity.stereotype, entity.name, entity.name.toLowerCase()],
    });
    // replace placeholder comments
    _.each(entity.attributes, async (attr, idx) => {
      await replace({
        files:[
                trgtCtrFilename
              ],
        from:[
              /\/\/__contractStructAttributes/g,
              /\/\/__contractAddParticipantNewStruct/g,
              /\/\/__contractAddParticipantParams/g,
              /\/\/__contractGetParticipantDeclareReturn/g,
              /\/\/__contractGetParticipantReturnAttribute/g,
              /\/\/__contractGetParticipantByAddressDeclareReturn/g,
              /\/\/__contractGetParticipantByAddressReturnAttribute/g,
              /\/\/__contractUpdateParticipantParams/g,
              /\/\/__contractUpdateParticipantDeclareReturn/g,
              /\/\/__contractUpdateParticipantAssign/g,
              /\/\/__contractUpdateParticipantReturn/g
            ],
        to:[
            ptr.__contractStructAttributesTo(attr, idx, parent),
            ptr.__contractAddParticipantNewStructTo(attr, idx, parent),
            ptr.__contractAddParticipantParamsTo(attr, idx, parent),
            ptr.__contractGetParticipantDeclareReturnTo(attr, idx, parent),
            ptr.__contractGetParticipantReturnAttributeTo(attr, idx, parent),
            ptr.__contractGetParticipantByAddressDeclareReturnTo(attr, idx, parent),
            ptr.__contractGetParticipantByAddressReturnAttributeTo(attr, idx, parent),
            ptr.__contractUpdateParticipantParamsTo(attr, idx, parent),
            ptr.__contractUpdateParticipantDeclareReturnTo(attr, idx, parent),
            ptr.__contractUpdateParticipantAssignTo(attr, idx, parent),
            ptr.__contractUpdateParticipantReturnTo(attr, idx, parent)
          ]
      });
      await replace({
        files:[
                trgtTstFilename
              ],
        from: [
                /\/\/__testAddParticipantParams/g,
                /\/\/__testGetParticipantAssertAttribute/g,
                /\/\/__testGetParticipantByAddressAssertAttribute/g
              ],
        to: [
              ptr.__testAddParticipantParamsTo(attr, idx, parent),
              ptr.__testGetParticipantAssertAttributeTo(attr, idx, parent),
              ptr.__testGetParticipantByAddressAssertAttributeTo(attr, idx, parent)
            ]
      })
    });
    var trgtDeployCtrContent = tmplDeployCtrContent.replace(/_EntityAssetType_/g, entity.name + entity.stereotype);
    if ( idx === 0 ) {
      await fs.writeFile(trgtDepCtrFilename, trgtDeployCtrContent);
    } else {
      await fs.appendFile(trgtDepCtrFilename,trgtDeployCtrContent);
    }
  } catch (err) {
      dialog.showMessageBox({
        type: "error",
        title: "Quorum Builder Generation",
        message: err.toString()
      });
  }
}
exports.add = add;
