var fs = require("fs-extra");
var path = require("path");
var read = require("read-data");
var replace = require("replace-in-file");
var config = require("../configUtil/config");

async function add(project, cfProviderDetails) {
  var providerDetailsJSON = config.readProviderJSON(cfProviderDetails);
  var providerName = config.readProviderName(cfProviderDetails,".json");

  var tmplProjectConfigFilename = config.readScriptPathSetting() + "/_template_truffle-config.js";
  var tmplProjectConfigContent = await fs.readFile(tmplProjectConfigFilename);

  // Target configuration file for project
  var trgtConfigFilename = project.generationProjectPath + "/truffle-config.js";
  await fs.writeFile(trgtConfigFilename,tmplProjectConfigContent.toString());
  await replace({
    files: [trgtConfigFilename],
    from: [/_providerName_/g, /_provider_rpcUrl/g, /_provider_networkID/g, /_provider_menemonic/g],
    to: [providerName, providerDetailsJSON.rpc, providerDetailsJSON.networkId, providerDetailsJSON.mnemonic],
  });
}

exports.add = add;
