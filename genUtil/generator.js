var _ = require("underscore");
var config = require("../configUtil/config");
var statusChecker = require("../projUtil/statusChecker");
var truffleInitator = require("./truffleInitiator");
var truffleCompiler = require("./truffleCompiler");
var truffleConfigurator = require("./truffleConfigurator");
var truffleMigrator = require("./truffleMigrator");
var truffleTester = require("./truffleTester");
var nodeInstaller = require("./nodeInstaller");
var participant = require("./participant");

async function generateProject(projectName) {
  var project = config.getProject(projectName);
  var model = config.readModelParsedJSON(projectName);
  if ( !statusChecker.isGenerationReady(project.status) ) { return;}
  await truffleInitator.init(project);
  await nodeInstaller.installModules(project);
  _.each(model.entities, async (entity, idx) => {
    switch(entity.stereotype) {
      case "Participant":
        participant.add(entity, idx, project);
        break;
      case "Asset":
        asset.add(entity, idx, project);
        break;
    }
  });
  await truffleCompiler.compile(project);
  config.changeProjectStatus(projectName, statusChecker.statusEnum.properties[statusChecker.statusEnum.GENERATED].name);
}

function resetProjectGeneration(projectName) {
  config.changeProjectStatus(projectName, statusChecker.statusEnum.properties[statusChecker.statusEnum.MODELED].name);
}

async function deployProjectDev(projectName, cfProviderDetails) {
  var project = config.getProject(projectName);
  if ( !statusChecker.isDeployDevReady(project.status) ) { return;}
  await truffleConfigurator.add(project, cfProviderDetails);
  await truffleMigrator.migrate(project,"development");
  await truffleTester.test(project, "development");
  config.changeProjectStatus(projectName, statusChecker.statusEnum.properties[statusChecker.statusEnum.DEVELOPMENT_DEPLOYED].name);
}

async function deployProjectPro(projectName, cfProviderDetails) {
  var project = config.getProject(projectName);
  var providerName = config.readProviderName(cfProviderDetails);
  // if ( !statusChecker.isDeployProReady(project.status) ) { return;}
  if ( statusChecker.getStatusValue(project.status) < statusChecker.statusEnum.DEVELOPMENT_DEPLOYED) { return;}
  await truffleMigrator.migrate(project, providerName);
  await truffleTester.test(project, providerName);
  config.addDeployedProviderName(projectName, cfProviderDetails);
  config.changeProjectStatus(projectName, statusChecker.statusEnum.properties[statusChecker.statusEnum.PROVIDER_MIGRATED].name);
}

exports.generateProject = generateProject;
exports.deployProjectDev = deployProjectDev;
exports.deployProjectPro = deployProjectPro;
exports.resetProjectGeneration = resetProjectGeneration;
