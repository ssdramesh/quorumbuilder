var path = require("path");
var _ = require("underscore");

function parseCanvasToSimplifiedJSON(canvasObject, filePath){
  var parsedJSON = {};
  parsedJSON.networkName=path.basename(filePath, ".json");
  parsedJSON.entities=[];
  var entities = _.where(canvasObject.objects,{type:"group"});
  _.each(entities, function(entity){
    var entityTitle = _.where(entity.objects,{type:"i-text"})[0].text;
    entityName = entityTitle.split("\n")[1];
    var stereotype = entityTitle.split("\n")[0];
    stereotype = stereotype.replace("<<","").replace(">>","");
    var entityAttributesArray = _.map(_.where(entity.objects,{type:"i-text"})[1].text.split("\n"), function(key){
      return {"name":key};
    });
    parsedJSON.entities.push({
      "name":entityName,
      "stereotype": stereotype,
      "attributes": entityAttributesArray
    });
  })
  return {
    "modelsFolder": path.dirname(filePath),
    "modelJSONFile": filePath,
    "modelParsedJSONFile": path.dirname(filePath) + "/" + parsedJSON.networkName + "Parsed" + ".json",
    "parsedJSON": parsedJSON
  }
}
exports.parseCanvasToSimplifiedJSON = parseCanvasToSimplifiedJSON;
