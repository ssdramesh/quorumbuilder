var {dialog, app} = require('electron').remote;
var _ = require("underscore");

const statusEnum = {
  NEW: 1,
  MODELED: 2,
  GENERATED: 3,
  DEVELOPMENT_DEPLOYED: 4,
  PROVIDER_MIGRATED: 5,
  properties: {
    1: {
      name: "new",
      value: 1,
      msg: "Project is in status 'new'",
      msgErr: "Project is not in status 'new'",
      msgExp: "Project is expected to be in status 'new'"
    },
    2: {
      name: "modeled",
      value: 2,
      msg: "Project is in status 'modeled'",
      msgErr: "Project is not in status 'modeled'",
      msgExp: "Project is expected to be in status 'modeled'"
    },
    3: {
      name: "generated",
      value: 3,
      msg: "Project is in status 'generated'",
      msgErr: "Project is not in status 'generated'",
      msgExp: "Project is expected to be in status 'generated'"
    },
    4: {
      name: "development-deployed",
      value: 4,
      msg: "Project is in status 'development-deployed'",
      msgErr: "Project is not in status 'development-deployed'",
      msgExp: "Project is expected to be in status 'development-deployed'"
    },
    5: {
      name: "provider-migrated",
      value: 5,
      msg: "Project is in status 'provider-migrated'",
      msgErr: "Project is not in status 'provider-migrated'",
      msgExp: "Project is expected to be in status 'provider-migrated'"
    },
  }
}

function getStatusValue(status) {
  return _.findWhere(statusEnum.properties, {name:status}).value;
}

function isModelingReady(status) {
  if ( status !== statusEnum.properties[statusEnum.NEW].name ) {
    dialog.showMessageBox({
      type: "error",
      title: "Quorum Builder Modeling",
      message: statusEnum.properties[statusEnum.NEW].msgErr + "\n" + statusEnum.properties[statusEnum.NEW].msgExp
    });
    return false;
  }
  return true;
}

function isGenerationReady(status) {
  if ( status !== statusEnum.properties[statusEnum.MODELED].name ) {
    dialog.showMessageBox({
      type: "error",
      title: "Quorum Builder Generation",
      message: statusEnum.properties[statusEnum.MODELED].msgErr + "\n" + statusEnum.properties[statusEnum.MODELED].msgExp
    });
    return false;
  }
  return true;
}

function isDeployDevReady(status) {
  if ( status !== statusEnum.properties[statusEnum.GENERATED].name ) {
    dialog.showMessageBox({
      type: "error",
      title: "Quorum Builder Deployment (Development)",
      message: statusEnum.properties[statusEnum.GENERATED].msgErr + "\n" + statusEnum.properties[statusEnum.GENERATED].msgExp
    });
    return false;
  }
  return true;
}

function isDeployProReady(status) {
  if ( status !== statusEnum.properties[statusEnum.DEVELOPMENT_DEPLOYED].name ) {
    dialog.showMessageBox({
      type: "error",
      title: "Quorum Builder Deployment (Provider)",
      message: statusEnum.properties[statusEnum.DEVELOPMENT_DEPLOYED].msgErr + "\n" + statusEnum.properties[statusEnum.DEVELOPMENT_DEPLOYED].msgExp
    });
    return false;
  }
  return true;
}

exports.isModelingReady = isModelingReady;
exports.isGenerationReady = isGenerationReady;
exports.isDeployDevReady = isDeployDevReady;
exports.isDeployProReady = isDeployProReady;
exports.getStatusValue = getStatusValue;
exports.statusEnum = statusEnum;
