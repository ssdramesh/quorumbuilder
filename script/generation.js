var read = require("read-data").sync;
var fs = require("fs-extra");
var generator = require("./genUtil/generator.js");

fillAllSelections();

function fillProjectSelections() {
    var projectName = document.getElementById("projectName");
    read("./config/config.json").projects.forEach(project => {projectName.options[projectName.options.length] = new Option(project.projectName, project.projectName);});
}

function fillProviderSelections() {
    var cfProviderDetails = document.getElementById("cfProviderDetails");
    fs.readdirSync("./_Templates/_Deployment/ProviderDetails").forEach(file => {cfProviderDetails.options[cfProviderDetails.options.length] = new Option(file, file);});
}

function fillAllSelections() {
  fillProjectSelections();
  fillProviderSelections();
}
/*
Add event listener for refresh projects button
 */
document.getElementById("refreshProjectList").addEventListener("click", () => {fillProjectSelections();});
/*
Add event listener for refresh cloud foundry channel details button
 */
document.getElementById("refreshCfProviderDetails").addEventListener("click", () => {fillProviderSelections();});
/*
Add event listener for generate project button
 */
document.getElementById("generateProject").addEventListener("click", () => {
  var projectName = document.getElementById("projectName");
  generator.generateProject(projectName.options[projectName.selectedIndex].value);
});
/*
Add event listener for reset project generation
 */
document.getElementById("resetProjectGeneration").addEventListener("click", () => {
  var projectName = document.getElementById("projectName");
  generator.resetProjectGeneration(projectName.options[projectName.selectedIndex].value);
})
/*
Add event listener for deploy project (network: 'developent') button
 */
document.getElementById("deployProjectDev").addEventListener("click", () => {
  var projectName = document.getElementById("projectName");
  generator.deployProjectDev(projectName.options[projectName.selectedIndex].value, cfProviderDetails.options[cfProviderDetails.selectedIndex].value);
});
/*
Add event listener for deploy project (network: specified provider) button
 */
document.getElementById("deployProjectPro").addEventListener("click", () => {
  var projectName = document.getElementById("projectName");
  generator.deployProjectPro(projectName.options[projectName.selectedIndex].value, cfProviderDetails.options[cfProviderDetails.selectedIndex].value);
});
