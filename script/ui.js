var read = require("read-data").sync;
var uiGenerator = require("./genUtil/generator.js");

fillAllSelections();

function fillProjectSelections() {
    var projectName = document.getElementById("uiProjectName");
    read("./config/config.json").projects.forEach(project => {uiProjectName.options[uiProjectName.options.length] = new Option(project.projectName, project.projectName);});
}

function fillAllSelections() {
  fillProjectSelections();
}
/*
Add venet listener for deploy setup UI5
 */
// document.getElementById("deploySetupUI").addEventListener("click", () => {
//   var projectName = document.getElementById("uiProjectName");
//   uiGenerator.deploySetupUI(projectName.options[projectName.selectedIndex].value);
// });
/*
Add event listener for refresh project list
 */
document.getElementById("refreshProjectsList").addEventListener("click", () => {
  fillProjectSelections();
})
