var path = require("path");
var configUtil = require("./configUtil/config.js")

function getSelectedDirectory() {
  mainProcess.selectDirectory();
}

// Fill table of existing projects
fillTableProjects();

/*
Fills table with existing projects
 */
function fillTableProjects() {
  var table = document.getElementById("tableProjects");
  var tableProjects = document.createElement("table");
  addProjectTableHead(tableProjects);
  var projects = configUtil.getProjects();
  if ( projects.length > 0 ){
    for (var i = 0; i < projects.length; i++) {
      addProjectTableRow(tableProjects, projects[i].projectName, projects[i].status)
    }
  } else {
    addProjectTableRow(tableProjects, "no existing projects", "add a new project")
  }
  table.appendChild(tableProjects);
}
/*
Fills existing projects table header
 */
function addProjectTableHead(tableProjects) {
  var head = document.createElement("thead");
  var headerRow = document.createElement("tr");
  var cellProjectName = document.createElement("td");
  var cellProjectStatus = document.createElement("td");
  var cellProjectAction = document.createElement("td");
  cellProjectName.appendChild(document.createTextNode("Name"));
  cellProjectStatus.appendChild(document.createTextNode("Status"));
  cellProjectAction.appendChild(document.createTextNode("Action"))
  headerRow.appendChild(cellProjectName);
  headerRow.appendChild(cellProjectStatus);
  headerRow.appendChild(cellProjectAction);
  head.appendChild(headerRow);
  tableProjects.appendChild(head);
}
/*
Build and add one row each for existing projects in the configurations table
 */
function addProjectTableRow(tableProjects, projectName, projectStatus) {
  var delButton = document.createElement("Button");
  delButton.id = projectName;
  delButton.innerHTML = "Delete";
  delButton.addEventListener("click", function() {
    deleteProject(projectName);
  })

  var row = document.createElement("tr");
  var cellProjectName = document.createElement("td");
  var cellProjectStatus = document.createElement("td");
  var cellProjectAction = document.createElement("td");
  cellProjectName.appendChild(document.createTextNode(projectName));
  cellProjectStatus.appendChild(document.createTextNode(projectStatus));
  cellProjectAction.appendChild(delButton);
  row.appendChild(cellProjectName);
  row.appendChild(cellProjectStatus);
  if (projectStatus !== "add a new project") {
    row.appendChild(cellProjectAction);
  } else
  {
    row.appendChild(document.createTextNode(""));
  }

  tableProjects.appendChild(row);
}

function deleteProject(projectName) {
  var status = configUtil.getProjectStatus(projectName);
  if ( status === "modeled" || status === "generated" ) {
    fs.removeSync(configUtil.getProject(projectName).modelsFolder);
    configUtil.removeProject(projectName);
    return;
  }
  if ( status === "generated" ) {
    fs.removeSync(configUtil.getProject(projectName).modelsFolder);
    fs.removeSync(configUtil.getProject(projectName).generationProjectPath);
    configUtil.removeProject(projectName);
    return;
  }
  if ( status === "deployed" ) {
    dialog.showErrorBox({
      title: "Delete Project",
      buttonLabel: "OK",
      message: "ERROR: Project " + projectName + " is already in status: " + status + "\n Cannot be deleted!"
    });
  }
  if ( status === "ready to re-deploy" ) {
    dialog.showErrorBox({
      title: "Delete Project",
      buttonLabel: "OK",
      message: "ERROR: Project " + projectName + " is already in status: " + status + "\n Cannot be deleted!"
    });
  }
  var tableProjects = document.getElementById("tableProjects")
  while(tableProjects.hasChildNodes()) {
     tableProjects.removeChild(tableProjects.firstChild);
  }
  fillTableProjects();
  dialog.showMessageBox({
    title: "Delete Project",
    buttonLabel: "Delete",
    message: "Project: " + projectName + "Successfully Deleted!"
  });
}

/*
Add event listener for project folder selection
 */
document.getElementById("selectNewProjectPath").addEventListener("click", function() {
  var selPath = dialog.showOpenDialog({
    title:"Select Project Location",
    buttonLabel: "Select",
    properties:["openDirectory"]
  });
  document.getElementById("selectedNewProjectPath").value = selPath;
})

/*
Add new project
 */
document.getElementById("addNewProject").addEventListener("click", () => {
  var projectName = document.getElementById("newProjectName").value;
  var projectPath = document.getElementById("selectedNewProjectPath").value;

  if ( projectName === "" || projectPath === "" ) {
    dialog.showErrorBox("Incomplete Specification", "Specify all Parameters");
  }
  configUtil.addNewProject(projectName, projectPath);
  var tableProjects = document.getElementById("tableProjects")
  while(tableProjects.hasChildNodes()) {
     tableProjects.removeChild(tableProjects.firstChild);
  }
  fillTableProjects();
});

/*
Add event listener for refresh
 */
document.getElementById("refreshProjects").addEventListener("click", function() {
  fillTableProjects();
})
