var {dialog} = require('electron').remote;
var electronDirectory = require("electron-directory");
var fs = require("fs-extra");
var write = require("write-data").sync;
var confConfigUtil = require("./configUtil/config.js");

document.getElementById("scriptsLocation").value = confConfigUtil.readScriptPathSetting();
document.getElementById("modelsLocation").value = confConfigUtil.readModelPathSetting();

function getSelectedDirectory() {
  mainProcess.selectDirectory();
}

fillTableProviders();

function fillTableProviders() {
  var table = document.getElementById("tableProviders");
  var tableProviders = document.createElement("table");
  addProviderTableHead(tableProviders);
  electronDirectory(__dirname)
    .then( function(electronDirectoryInstance) {
      electronDirectoryInstance.getApplicationPath("/_Templates/_Deployment/ProviderDetails")
        .then( function(appPath) {
          fs.readdirSync(appPath).forEach(file => {
            addProviderTableRow(tableProviders, file)
        })
        table.appendChild(tableProviders);
      });
    })
}
/*
Fills existing projects table header
 */
function addProviderTableHead(tableProviders) {
  var head = document.createElement("thead");
  var headerRow = document.createElement("tr");
  var cellProviderName = document.createElement("td");
  var cellProviderAction = document.createElement("td");
  cellProviderName.appendChild(document.createTextNode("Provider Name"));
  cellProviderAction.appendChild(document.createTextNode("Action"))
  headerRow.appendChild(cellProviderName);
  headerRow.appendChild(cellProviderAction);
  head.appendChild(headerRow);
  tableProviders.appendChild(head);
}
/*
Build and add one row each for existing projects in the configurations table
 */
function addProviderTableRow(tableProviders, providerName) {
  var delButton = document.createElement("Button");
  delButton.id = providerName;
  delButton.innerHTML = "Delete";
  delButton.addEventListener("click", function() {
    deleteProvider(providerName);
  })
  var row = document.createElement("tr");
  var cellProviderName = document.createElement("td");
  var cellProviderAction = document.createElement("td");
  cellProviderName.appendChild(document.createTextNode(providerName));
  cellProviderAction.appendChild(delButton);
  row.appendChild(cellProviderName);
  row.appendChild(cellProviderAction);
  tableProviders.appendChild(row);
}

function createProvider(providerName, providerJSON, providerMnemonic){
  var providersFolder = confConfigUtil.readScriptPathSetting() + "/_Deployment/ProviderDetails";
  var parsedProviderJSON = JSON.parse(providerJSON);
  parsedProviderJSON.mnemonic = providerMnemonic;
  write(providersFolder + "/" + providerName, parsedProviderJSON);
}

function deleteProvider(providerName) {
  var providersFolder = confConfigUtil.readScriptPathSetting() + "/_Deployment/ProviderDetails";
  fs.unlinkSync(providersFolder + "/" + providerName);
  var tableProviders = document.getElementById("tableProviders")
  while(tableProviders.hasChildNodes()) {
     tableProviders.removeChild(tableProviders.firstChild);
  }
  fillTableProviders();
  dialog.showMessageBox({
    title: "Delete Provider",
    buttonLabel: "Delete",
    message: "Provider Deleted" + providerName
  });
}

/*
Add new provider
 */
document.getElementById("addNewProviderWithKey").addEventListener("click", () => {
  var providerName = document.getElementById("newProviderName").value;
  var providerJSON = document.getElementById("newProviderKey").value;
  var providerMnemonic = document.getElementById("newProviderMnemonic").value;
  if ( providerName === "" || providerJSON === "" || providerMnemonic === "") {dialog.showErrorBox("Incomplete Specification", "Specify all Parameters");}
  createProvider(providerName, providerJSON, providerMnemonic);
});
